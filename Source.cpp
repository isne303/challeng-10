#include <iostream>
#include "list.h"

using namespace std;

void main()
{
	List x;
	
	//x.headPush(1); 
	//x.headPush(2);	
	//x.headPush(3);
	//x.headPush(4);
	x.tailPush(5);
	//x.deleteNode(3);
	x.headPop();
	x.tailPop();
	cout << "\nIs this number in list : ";
	int v;
	cin >> v;
	if (x.isInList(v) == true) {
		cout << "We found it \n";
	}
	else {
		cout << "Not found!!!\n";
	}
			
	system("PAUSE");
}