#include <iostream>
#include "list.h"

using namespace std;



List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
void List::headPush(int el) //Add element to front of list
{
	Node *p = new Node(el);
	if (isEmpty())
	{
		head = p;
		tail = p;
	}
	else
	{
		p->next = head;
		head = p;
	}
	cout << p->info;
}
void List::tailPush(int el)
{
	Node *p = new Node(el);
	if (isEmpty())
	{
		head = p;
		tail = p;
	}
	else
	{
		tail->next = p;
		tail = p;
	}
	cout << p->info;
}

bool List::isInList(int el) {
	Node* p;
	if (!isEmpty()) { //case empty value.

	p = head;	// Start from the first value.
	while (p->info != el) { // look at first element if that same as "el" value.
		
		if (p->next == NULL) {		// if next element is empty so dont have to move pointer p to check next element
			return false;			// so we return false.  <3
		}
		p = p->next; //in case we have next element so we must move pointer before new check func.
	}
	return true; // In case we found it!
	}
	return false;	
}

void List::deleteNode(int el) {
	Node* p= head;// Start from the first value.

	if (!isEmpty()) {
		while (p->info != el) {

			if (p->next == NULL) { //in case have no one node.
				break;
			}
			p = p->next;	// move checkerptr.
			
		}
		// we found it.


		if (p->info == el && p == head && head != tail) { // case delete head.
			Node* temp = head;
			head = head->next;
			cout <<" "<< temp->info;
			delete temp;
			
		
		}
		else if (p->info == el && p == tail && head != tail) { // case delete tail.
			Node* temp = head;
			while (temp->next->info != el ) { //find node which stay before target.
				temp = temp->next; // move pointer.
			}
			tail = temp;
			temp = temp->next;
			tail->next = NULL;
			cout <<" "<< temp->info;
			delete temp->next;
		}
		else if (p->info == el && p == tail && head == tail){ // case one element
			head = NULL;
			//cout << p->info;
			delete p;
		}
		else if (p->info == el) { //other case
			Node *temp = head;
			while (temp -> next != p)
			{
				temp = temp -> next;
			}
			temp -> next = p -> next;
			cout <<" "<< p ->info;
			delete p;
		}
	
	}
}

int List::headPop()
{
	Node *p = head;
	int re; // for get value of return
	if (isEmpty()) { return NULL; }
	if (!isEmpty() && head != tail) 
	{
		re = p -> info;
		head = p -> next;
		delete p;
		return re;
	}
	else if (!isEmpty() && head == tail) // case only one element
	{
		re = p -> info;
		head = p -> next;
		delete p;
		cout << re;
		return re;
	}
	return NULL;
}

int List::tailPop()
{
	Node *p = head;
	if (isEmpty()) { return NULL; }
	int re = tail->info;		// for get value of return
	if (!isEmpty() && head != tail)
	{
		while (p ->next ->next != NULL)
		{
			p = p ->next;
		}
		tail = p;
		p = tail->next;
		tail->next = NULL;
		delete p;
		return re; 
	}
	else if (!isEmpty() && head == tail) // case only one element
	{
		re = p->info;
		head = p->next;
		delete p;
		cout << re;
		return re;
	}
	return NULL;
}
